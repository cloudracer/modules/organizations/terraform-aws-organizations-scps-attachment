locals {
  scps_map = { for scp in var.scps : scp => scp }
}

resource "aws_organizations_policy_attachment" "selected" {
  for_each = local.scps_map

  policy_id = var.scp_resources[each.key].id # Policy
  target_id = var.target_id
}
