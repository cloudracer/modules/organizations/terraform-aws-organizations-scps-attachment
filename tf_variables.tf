variable "scps" {
  description = "List of SCP Names to apply to OU or Account"
  default     = []
}

variable "target_id" {
  type        = string
  description = "ID of an OU or Account"
}

variable "all_scps_names" {
  description = "List of all SCPS available"
  default     = []
}

variable "scp_resources" {
  description = "Resources of created SCPs"
}

# Other #
variable "tags" {
  description = "Tags for all AWS Resources of that Module"
  default     = {}
}
